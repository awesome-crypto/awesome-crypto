"use strict";

var config = {
    rateLimit: {
        "bittrex": 45,
        "binance": 100
    },
    server: "localhost",
    database: "crypto",
    exchange: process.env.exchange,
    interval: process.env.interval,
    intervalToExchangeInput: {
        "bittrex": {
            "1h": "hour",
            "1d": "day"
        },
        "binance": {
            "1h": "1h",
            "1d": "1d"
        }
    },
    intervalToDateTimeDiffMap: {
        "1h": "hours",
        "1d": "days"
    },
    intervalToCronExpressionMap: {
        "1h": "0 5 * * * *",
        "1d": "0 10 0 * * *",
    }
};

module.exports = config;
