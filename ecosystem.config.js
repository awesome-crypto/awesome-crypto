module.exports = {
    apps : [{
        name        : "worker-binance-1d-dev",
        script      : "~/node_projects/awesome-crypto/worker.js",
        watch       : false,
        env: {
            "exchange": "binance",
            "interval": "1d"
        }
    },{
        name        : "worker-binance-1h-dev",
        script      : "~/node_projects/awesome-crypto/worker.js",
        watch       : false,
        env: {
            "exchange": "binance",
            "interval": "1h"
        }
    },{
        name        : "worker-bittrex-1d-dev",
        script      : "~/node_projects/awesome-crypto/worker.js",
        watch       : false,
        env: {
            "exchange": "bittrex",
            "interval": "1d"
        }
    },{
        name        : "worker-bittrex-1h-dev",
        script      : "~/node_projects/awesome-crypto/worker.js",
        watch       : false,
        env: {
            "exchange": "bittrex",
            "interval": "1h"
        }
    },{
        name        : "worker-binance-1d",
        script      : "~/awesome-crypto/worker.js",
        watch       : false,
        env: {
            "exchange": "binance",
            "interval": "1d"
        }
    },{
        name        : "worker-binance-1h",
        script      : "~/awesome-crypto/worker.js",
        watch       : false,
        env: {
            "exchange": "binance",
            "interval": "1h"
        }
    },{
        name        : "worker-bittrex-1d",
        script      : "~/awesome-crypto/worker.js",
        watch       : false,
        env: {
            "exchange": "bittrex",
            "interval": "1d"
        }
    },{
        name        : "worker-bittrex-1h",
        script      : "~/awesome-crypto/worker.js",
        watch       : false,
        env: {
            "exchange": "bittrex",
            "interval": "1h"
        }
    }]
}
