# Awesome Crypto - Workers
Cron-jobs set to fetch OHLC market data from exchanges with defined time interval.
Supported exchanges;
 - binance
 - bittrex

and supported time intervals;
 - 1h
 - 1d

Will add more exchanges and time intervals later.
Each worker will persist market data into mongodb with schema;
```
{
    msi: String, // key of the document by combining exchange name, symbol and interval
    s: String, // symbol
    e: String, // exchange name
    i: String, // interval
    m: {
        o: Number,
        h: Number,
        l: Number,
        c: Number,
        t: Number,
        bv: Number,
        _id: false
    }
}
```

Example entity;

```
{
    "_id" : ObjectId("5b09d27cca945cc610341e84"),
    "m" : {
        "o" : 0.0007009,
        "h" : 0.00071,
        "l" : 0.000661,
        "c" : 0.0006653,
        "bv" : 556.26911862,
        "t" : 1527206400000
    },
    "msi" : "binance_BTC-STRAT_1d",
    "i" : "1d",
    "s" : "BTC-STRAT",
    "e" : "binance",
    "__v" : 0
}
```

Using pm2 as process manager. For more information - https://www.npmjs.com/package/pm2

To start all processes
> pm2 start all

To start an individual process
> pm2 start node_projects/awesome-crypto/ecosystem.config.js --only worker-binance-1h

```
┌───────────────────┬────┬──────┬──────┬────────┬─────────┬────────┬─────┬───────────┬──────────┬──────────┐
│ App name          │ id │ mode │ pid  │ status │ restart │ uptime │ cpu │ mem       │ user     │ watching │
├───────────────────┼────┼──────┼──────┼────────┼─────────┼────────┼─────┼───────────┼──────────┼──────────┤
│ worker-binance-1d │ 1  │ fork │ 9493 │ online │ 3       │ 0s     │ 10% │ 25.6 MB   │ ec2-user │ disabled │
│ worker-binance-1h │ 0  │ fork │ 9487 │ online │ 4       │ 0s     │ 23% │ 26.3 MB   │ ec2-user │ disabled │
│ worker-bittrex-1d │ 3  │ fork │ 9511 │ online │ 3       │ 0s     │ 6%  │ 23.8 MB   │ ec2-user │ disabled │
│ worker-bittrex-1h │ 2  │ fork │ 9495 │ online │ 6       │ 0s     │ 10% │ 25.1 MB   │ ec2-user │ disabled │
└───────────────────┴────┴──────┴──────┴────────┴─────────┴────────┴─────┴───────────┴──────────┴──────────┘
```

## Using

 - npm v4.0.5
 - node v9.8.0
 - mongodb 3.6