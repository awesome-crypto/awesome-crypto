"use strict";

var assert = require('assert');
var sinon = require('sinon');
var proxyquire = require('proxyquire').noCallThru();
var Log = require('log');

describe("ExchangeHandlerFactory", function() {
    var loggerFactory;
    var queryHelper;
    var exchangeHandler;
    var binanceHandlerInstance;
    var bittrexHandlerInstance;
    var BinanceHandler;
    var BittrexHandler;
    var exchangeHandlerFactory;

    before(function() {
        initCommonStubs();
        initExchangeHandlerStubs();
        exchangeHandlerFactory = proxyquire('../../src/factory/exchange-factory.js',
                                            {
                                                '../handlers/exchange/bittrex-handler.js': BittrexHandler,
                                                '../handlers/exchange/binance-handler.js': BinanceHandler
                                            });
    });

    after(function() {
        loggerFactory.getDefaultLogger.restore();
    });

    describe('#getExchangeHandler', function() {
        it("should return bittrexHandler for bittrex exchange", function() {
            var actualHandler = exchangeHandlerFactory.getExchangeHandler("bittrex");
            assert.ok(actualHandler, bittrexHandlerInstance);
        });

        it("should return binance handler for binance exchange", function() {
            var actualHandler = exchangeHandlerFactory.getExchangeHandler("binance");
            assert.ok(actualHandler, binanceHandlerInstance);
        });

        it("should return same handler instance for every request", function() {
            var firstHandler = exchangeHandlerFactory.getExchangeHandler("binance");
            var secondHandler = exchangeHandlerFactory.getExchangeHandler("binance");
            assert.equal(firstHandler, secondHandler);
        });
    });

    function initCommonStubs() {
        loggerFactory = proxyquire('../../src/log/logger.js', {});
        var logStub = sinon.createStubInstance(Log);
        sinon.stub(loggerFactory, "getDefaultLogger").returns(logStub);

        queryHelper = proxyquire('../../src/handlers/query-helper.js',
                                            {'../log/logger.js': loggerFactory});

        exchangeHandler = proxyquire('../../src/handlers/exchange/exchange-handler.js',
                                            {'../../log/logger.js': loggerFactory,
                                                '../query-helper.js': queryHelper
                                            });
    }

    function initExchangeHandlerStubs() {
        initBinance();
        initBittrex();
    }

    function initBittrex() {
        BittrexHandler = proxyquire('../../src/handlers/exchange/bittrex-handler.js',
                                            {'./exchange-handler.js': exchangeHandler,
                                                 '../../log/logger.js': loggerFactory
                                            });
        bittrexHandlerInstance = new BittrexHandler("1h");
    }

    function initBinance() {
        BinanceHandler = proxyquire('../../src/handlers/exchange/binance-handler.js',
                                            {'./exchange-handler.js': exchangeHandler,
                                                '../query-helper.js': queryHelper,
                                                '../../log/logger.js': loggerFactory
                                            });
        binanceHandlerInstance = new BinanceHandler("1h");
    }
});