"use strict";

var assert = require("assert");
var Utils = require("../../src/handlers/utils.js");
var luxon = require("luxon");
var DateTime = luxon.DateTime;

describe("Utils", function() {
    describe("#getDifferenceByInterval()", function() {
        it("time difference should be 2 hours", function() {
            var first = DateTime.fromISO("2018-03-10T23:00:00Z", {zone: "utc"});
            var second = DateTime.fromISO("2018-03-11T01:00:00Z", {zone: "utc"});
            var diff =  Utils.getDifferenceByInterval(first, second, "hours");
            assert.equal(2, diff);
        });

        it("time difference should be 1 hour", function() {
            var first = DateTime.fromISO("2018-03-10T23:00:00Z", {zone: "utc"});
            var second = DateTime.fromISO("2018-03-11T00:00:00Z", {zone: "utc"});
            var diff =  Utils.getDifferenceByInterval(first, second, "hours");
            assert.equal(1, diff);
        });

        it("time difference should be -1 hour", function() {
            var first = DateTime.fromISO("2018-03-10T23:00:00Z", {zone: "utc"});
            var second = DateTime.fromISO("2018-03-10T22:00:00Z", {zone: "utc"});
            var diff =  Utils.getDifferenceByInterval(first, second, "hours");
            assert.equal(-1, diff);
        });

        it("time difference should be less than 1 hour", function() {
            var first = DateTime.fromISO("2018-03-10T23:15:00Z", {zone: "utc"});
            var second = DateTime.fromISO("2018-03-11T00:00:00Z", {zone: "utc"});
            var diff =  Utils.getDifferenceByInterval(first, second, "hours");
            assert.ok(diff < 1);
        });

        it("time difference should throw error if first date param is null", function() {
            assert.throws(function() {
                var first = null;
                var second = DateTime.fromISO("2018-03-11T00:00:00Z", {zone: "utc"});
                Utils.getDifferenceByInterval(first, second, "hours");
            });
        });

        it("time difference should throw error if second date param is null", function() {
            assert.throws(function() {
                var first = DateTime.fromISO("2018-03-11T00:00:00Z", {zone: "utc"});
                var second = null;
                Utils.getDifferenceByInterval(first, second, "hours");
            });
        });

        it("date difference should be 1", function() {
            var first = DateTime.fromISO("2018-04-12T00:00:00Z", {zone: "utc"});
            var second = DateTime.fromISO("2018-04-13T00:00:00Z", {zone: "utc"});
            var diff =  Utils.getDifferenceByInterval(first, second, "days");
            assert.equal(1, diff);
        });
    });

    describe("#getPrevDataListWithMillis()", function() {
        var oneHourDataSet;
        var oneDayDataSet;

        before(function() {
            oneHourDataSet = [{
                o: 1234,
                h: 5678,
                l: 8907,
                c: 1111,
                t: 1523206800000
            },
            {
                o: 1234,
                h: 5678,
                l: 8907,
                c: 1111,
                t: 1523210400000
            },
            {
                o: 1234,
                h: 5678,
                l: 8907,
                c: 1111,
                t: 1523214000000
            },
            {
                o: 1234,
                h: 5678,
                l: 8907,
                c: 1111,
                t: 1523217600000
            }];

            oneDayDataSet = [{
                o: 0.00003933,
                h: 0.00004335,
                l: 0.00003821,
                c: 0.00004036,
                t: 1472774400000,
                v: 37.94143689
            },{
                o: 0.00003933,
                h: 0.00004335,
                l: 0.00003821,
                c: 0.00004036,
                t: 1472860800000,
                v: 37.94143689
            },{
                o: 0.00003933,
                h: 0.00004335,
                l: 0.00003821,
                c: 0.00004036,
                t: 1472947200000,
                v: 37.94143689
            },{
                o: 0.00003933,
                h: 0.00004335,
                l: 0.00003821,
                c: 0.00004036,
                t: 1473033600000,
                v: 37.94143689
            }];
        });

        it("get previous last data set contains 1 elements to be added for hourly interval", function() {
            var lastEntityDateTime = DateTime.fromMillis(1523214000000, {zone: "UTC"});
            var prevData = Utils.getPrevDataListWithMillis(lastEntityDateTime, oneHourDataSet, "hours");
            assert.equal(prevData.length, 1);
        });

        it("get previous last data set contains 0 elements to be added for hourly interval", function() {
            var lastEntityDateTime = DateTime.fromMillis(1523217600000, {zone: "UTC"});
            var prevData = Utils.getPrevDataListWithMillis(lastEntityDateTime, oneHourDataSet, "hours");
            assert.equal(prevData.length, 0);
        });

        it("get previous last data set contains 3 elements to be added for hourly interval", function() {
            var lastEntityDateTime = DateTime.fromMillis(1523206800000, {zone: "UTC"});
            var prevData = Utils.getPrevDataListWithMillis(lastEntityDateTime, oneHourDataSet, "hours");
            assert.equal(prevData.length, 3);
        });

        it("get previous last data set contains 1 elements to be added for daily interval", function() {
            var lastEntityDateTime = DateTime.fromMillis(1472947200000, {zone: "UTC"});
            var prevData = Utils.getPrevDataListWithMillis(lastEntityDateTime, oneDayDataSet, "days");
            assert.equal(prevData.length, 1);
        });

        it("get previous last data set contains 0 elements to be added for daily interval", function() {
            var lastEntityDateTime = DateTime.fromMillis(1473033600000, {zone: "UTC"});
            var prevData = Utils.getPrevDataListWithMillis(lastEntityDateTime, oneDayDataSet, "days");
            assert.equal(prevData.length, 0);
        });

        it("get previous last data set contains 3 elements to be added for daily interval", function() {
            var lastEntityDateTime = DateTime.fromMillis(1472774400000, {zone: "UTC"});
            var prevData = Utils.getPrevDataListWithMillis(lastEntityDateTime, oneDayDataSet, "days");
            assert.equal(prevData.length, 3);
        });
    });
});
