"use strict";

var assert = require("assert");
var sinon = require("sinon");
var proxyquire = require("proxyquire").noCallThru();
var Log = require("log");

describe("BittrexHandler", function() {
    var bittrexHandler;
    var exchangeHandler;
    var config;
    var loggerFactory;
    var queryHelper;
    var getFirstBySortedStub;
    var requestStub;
    var marketData = [
        {
            O: 0.00003933,
            H: 0.00004335,
            L: 0.00003821,
            C: 0.00004036,
            V: 944636.10362335,
            T: "2016-09-02T00:00:00",
            BV: 37.94143689
        }
    ];

    before(function() {
        config = {
            exchange: "bittrex",
            interval: "1h",
            intervalToExchangeInput: {
                "bittrex": {
                    "1h": "hour",
                    "1d": "day"
                },
                "binance": {
                    "1h": "1h",
                    "1d": "1d"
                }
            }
        };

        loggerFactory = proxyquire("../../../src/log/logger.js", {"../../config.js": config});
        var logStub = sinon.createStubInstance(Log);
        sinon.stub(loggerFactory, "getDefaultLogger").returns(logStub);

        queryHelper = proxyquire("../../../src/handlers/query-helper.js",
                                    {"../log/logger.js": loggerFactory});
        getFirstBySortedStub = sinon.stub(queryHelper, "getFirstBySorted");

        exchangeHandler = proxyquire("../../../src/handlers/exchange/exchange-handler.js",
                                        {"../../log/logger.js": loggerFactory,
                                            "../query-helper.js": queryHelper,
                                            "../../../config.js": config});
        requestStub = sinon.stub();
        var BittrexHandler = proxyquire("../../../src/handlers/exchange/bittrex-handler.js",
                                        {"./exchange-handler.js": exchangeHandler,
                                            "../../../config.js": config,
                                            "../../log/logger.js": loggerFactory,
                                            "request": requestStub});
        bittrexHandler = new BittrexHandler("1h");
    });

    after(function() {
        loggerFactory.getDefaultLogger.restore();
    });

    describe("#buildGetMarketTickerUrl()", function() {
        it("should return correct bittrex market ticker url", function() {
            assert.equal(bittrexHandler.buildGetMarketTickerUrl("VTC"),
                         "https://bittrex.com/Api/v2.0/pub/market/GetTicks?marketName=BTC-VTC&tickInterval=hour");
        });
    });

    describe("#buildSymbolParam()", function() {
        it("should return correct symbol param", function() {
            assert.equal(bittrexHandler.buildSymbolParam("VTC"), "BTC-VTC");
        });
    });

    describe("#getMarketsUrl()", function() {
        it("should return correct getMarketsUrl", function() {
            assert.equal(bittrexHandler.getMarketsUrl(), "https://bittrex.com/api/v1.1/public/getmarkets");
        });
    });

    describe("#convertResponseToCommonModel()", function() {
        it("should return market data in correct schema format ", function() {
            var marketDataList = bittrexHandler.convertResponseToCommonModel(marketData);
            assert.equal(marketDataList.length, 1);
            assert.equal(marketDataList[0].o, marketData[0].O);
            assert.equal(marketDataList[0].h, marketData[0].H);
            assert.equal(marketDataList[0].l, marketData[0].L);
            assert.equal(marketDataList[0].c, marketData[0].C);
            assert.equal(marketDataList[0].t, 1472774400000);
        });
    });

    describe("#getMarketList()", function() {
        it("should return market list", function() {
            var marketListResponse = {
                result: [
                    {
                        IsActive: true,
                        BaseCurrency: "BTC",
                        MarketCurrency: "STRAT"
                    },
                    {
                        IsActive: true,
                        BaseCurrency: "BTC",
                        MarketCurrency: "VTC"
                    }
                ]
            };
            requestStub.withArgs(sinon.match.any, sinon.match.any)
                        .yields(null, null, JSON.stringify(marketListResponse));
            var resultPromise = bittrexHandler.getMarketList();
            return resultPromise.then(function(resultMarketList) {
                assert.equal("STRAT", resultMarketList[0]);
                assert.equal("VTC", resultMarketList[1]);
            });
        });
    });

    describe("#requestMarketData()", function() {
        it("should return market data", function() {
            var requestResult = {
                result: marketData
            };
            requestStub.withArgs(sinon.match.any, sinon.match.any).yields(null, null, JSON.stringify(requestResult));
            var resultPromise = bittrexHandler.requestMarketData("STRAT");
            return resultPromise.then(function(resultMarketData) {
                assert.equal(resultMarketData[0].O, 0.00003933);
                assert.equal(resultMarketData[0].H, 0.00004335);
                assert.equal(resultMarketData[0].L, 0.00003821);
                assert.equal(resultMarketData[0].C, 0.00004036);
            });
        });

        it("should return empty result if parsing response body fails", function() {
            requestStub.withArgs(sinon.match.any, sinon.match.any).yields(null, null, "<HTML>");
            var resultPromise = bittrexHandler.requestMarketData("STRAT");
            return resultPromise.then(function(resultMarketData) {
                assert.ok(resultMarketData.length === 0);
            });
        });
    });
});
