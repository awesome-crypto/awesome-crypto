"use strict";

var assert = require("assert");
var sinon = require("sinon");
var proxyquire = require("proxyquire").noCallThru();
var Log = require("log");

describe("BinanceHandler", function() {
    var binanceHandler;
    var exchangeHandler;
    var config;
    var loggerFactory;
    var queryHelper;
    var getFirstBySortedStub;
    var requestStub;
    var marketData;

    before(function() {
        config = {
            exchange: "binance",
            interval: "1h",
            intervalToExchangeInput: {
                "bittrex": {
                    "1h": "hour",
                    "1d": "day"
                },
                "binance": {
                    "1h": "1h",
                    "1d": "1d"
                }
            }
        };

        marketData = [[
            1524945600000,
            "0.00069350",
            "0.00069660",
            "0.00068340",
            "0.00069480",
            "75731.42000000",
            1524949199999,
            "52.24443372",
            1512,
            "34054.84000000",
            "23.50594893",
            "0"
        ]];

        loggerFactory = proxyquire("../../../src/log/logger.js", {"../../config.js": config});
        var logStub = sinon.createStubInstance(Log);
        sinon.stub(loggerFactory, "getDefaultLogger").returns(logStub);

        queryHelper = proxyquire("../../../src/handlers/query-helper.js", 
                                {"../log/logger.js": loggerFactory});
        getFirstBySortedStub = sinon.stub(queryHelper, "getFirstBySorted");

        exchangeHandler = proxyquire("../../../src/handlers/exchange/exchange-handler.js",
                                        {"../../log/logger.js": loggerFactory,
                                            "../query-helper.js": queryHelper,
                                            "../../../config.js": config});
        requestStub = sinon.stub();
        var BinanceHandler = proxyquire("../../../src/handlers/exchange/binance-handler.js",
                                            {"./exchange-handler.js": exchangeHandler,
                                                "../query-helper.js": queryHelper,
                                                "../../../config.js": config,
                                                "../../log/logger.js": loggerFactory,
                                                "request": requestStub});
        binanceHandler = new BinanceHandler("1h");
    });

    after(function() {
        loggerFactory.getDefaultLogger.restore();
    });

    describe("#buildGetMarketTickerUrl()", function() {
        it("should return correct buildGetMarketTickerUrl with startTime and endTime params", function() {
            var lastData = {
                s: "BTC-VTC",
                i: "1h",
                e: "binance",
                m: {
                    "o" : 0.0006882,
                    "h" : 0.00071,
                    "l" : 0.0006882,
                    "c" : 0.0007008,
                    "bv" : 63.27676876,
                    "t" : 1524963600000
                }
            };
            getFirstBySortedStub.withArgs({exchange: "binance", interval: "1h", symbol: "BTC-VTC"}, -1)
                            .resolves(lastData);
            var promiseRes = binanceHandler.buildGetMarketTickerUrl("VTC");
            return promiseRes.then(function(marketUrl) {
                assert.equal(marketUrl,
                             "https://api.binance.com/api/v1/klines?symbol=VTCBTC&interval=1h&startTime=1524963600000");
            });
        });

        it("should return correct buildGetMarketTickerUrl", function() {
            getFirstBySortedStub.withArgs({exchange: "binance", interval: "1h", symbol: "BTC-VTC"}, -1)
                            .resolves(null);
            var promiseRes = binanceHandler.buildGetMarketTickerUrl("VTC");
            return promiseRes.then(function(marketUrl) {
                assert.equal(marketUrl,
                            "https://api.binance.com/api/v1/klines?symbol=VTCBTC&interval=1h");
            });
        });
    });

    describe("#requestMarketData()", function() {
        it("should return market data", function() {
            getFirstBySortedStub.withArgs({exchange: "binance", interval: "1h", symbol: "BTC-STRAT"}, -1)
                            .resolves(null);
            requestStub.withArgs(sinon.match.any, sinon.match.any).yields(null, null, JSON.stringify(marketData));
            var resultPromise = binanceHandler.requestMarketData("STRAT");
            return resultPromise.then(function(marketDataRes) {
                var data = marketData[0];
                assert.equal(data[0], 1524945600000);
                assert.equal(data[1], "0.00069350");
                assert.equal(data[2], "0.00069660");
                assert.equal(data[3], "0.00068340");
                assert.equal(data[4], "0.00069480");
            });
        });
    });

    describe("#buildSymbolParam()", function() {
        it("should return correct symbol param", function() {
            assert.equal(binanceHandler.buildSymbolParam("VTC"), "VTCBTC");
        });
    });

    describe("#convertResponseToCommonModel()", function() {
        it("should convert response to correct db schema", function() {
            var result = binanceHandler.convertResponseToCommonModel(marketData)[0];
            assert.equal(result.t, 1524945600000);
            assert.equal(result.o, "0.00069350");
            assert.equal(result.h, "0.00069660");
            assert.equal(result.l, "0.00068340");
            assert.equal(result.c, "0.00069480");
        });
    });

    describe("#getMarketList()", function() {
        it("should return market list", function() {
            var marketListResponse = {
                symbols: [
                    {
                        symbol: "ETHBTC",
                        baseAsset: "ETH",
                        quoteAsset: "BTC",
                        status: "TRADING"
                    },
                    {
                        symbol: "STRATBTC",
                        baseAsset: "STRAT",
                        quoteAsset: "BTC",
                        status: "TRADING"
                    }
                ]
            };
            requestStub.withArgs(sinon.match.any, sinon.match.any)
                        .yields(null, null, JSON.stringify(marketListResponse));
            var resultPromise = binanceHandler.getMarketList();
            return resultPromise.then(function(resultMarketList) {
                assert.equal("ETH", resultMarketList[0]);
            });
        });
    });
});
