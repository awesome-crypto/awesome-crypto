"use strict";

var assert = require('assert');
var sinon = require('sinon');
var proxyquire = require('proxyquire');
var Log = require('log');

describe('Logger', function() {
    var config;
    var loggerFactory;
    var _createDirStub;
    var logStub;

    before(function() {
        config = {
            exchange: 'binance',
            interval: '1h'
        };

        loggerFactory = proxyquire('../../src/log/logger.js', {'../../config.js': config});
        logStub = sinon.createStubInstance(Log);
        _createDirStub = sinon.stub(loggerFactory, "getDefaultLogger").returns(logStub);
    });

    after(function() {
        _createDirStub.restore();
    });

    describe('#getDefaultLogger()', function() {
        it('loggerFactory should return default logger', function() {
            var logger = loggerFactory.getDefaultLogger();
            assert.equal(logger, logStub);
        });
    });
});
