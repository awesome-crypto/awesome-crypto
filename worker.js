"use strict";

var async = require("async");
const cron = require("node-cron");
var logger = require("./src/log/logger.js").getDefaultLogger();
const config = require("./config.js");
var exchangeHandler = require("./src/factory/exchange-factory.js")
                        .getExchangeHandler(config.exchange, config.interval);

// init db and establish mongo connection
require("./src/database/database.js").initDb();

logger.info("Worker started with environment; exchange: " + config.exchange + " interval: " + config.interval);

var task = cron.schedule(config.intervalToCronExpressionMap[[config.interval]], async function() {
    var marketList = await exchangeHandler.getMarketList();
    async.mapLimit(marketList, config.rateLimit[config.exchange], function(market, callback) {
        setTimeout(async function() {
            try {
                var requestResult = await exchangeHandler.requestMarketData(market);
                callback(null, {
                    symbol: "BTC-" + market,
                    result: requestResult
                });
            } catch(err) {
                logger.error("requestMarketData for market: " + market + " failed with an error: " + err);
                callback(null, {
                    symbol: "BTC-" + market,
                    result: []
                });
            }
        }, 60000);
    }, function(err, marketDataList) {
        if (err) {
            logger.error("Error during async.map process. " + err);
            return;
        }

        marketDataList.forEach(function(marketData) {
            if (marketData.result === null) {
                logger.error("null for symbol: " + marketData.symbol);
                return;
            } else if (marketData.result.length === 0) {
                logger.error("empty result for symbol: " + marketData.symbol);
                return;
            }
            exchangeHandler.processResultMarketData(marketData);
        });
    });
});

task.start();
