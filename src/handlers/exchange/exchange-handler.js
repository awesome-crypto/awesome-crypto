"use strict";

const querystring = require("querystring");
const config = require("../../../config.js");
const logger = require("../../log/logger.js").getDefaultLogger();
var _ = require("lodash");
var QueryHelper = require("../query-helper.js");
var Utils = require("../utils.js");
var DateTime = require("luxon").DateTime;

/**
    Abstract handler class for exchange handlers.
    All exchange handlers have to extend this class.
*/
class ExchangeHandler {
    constructor(getMarketsUrl, getMarketDataBaseUrl, interval) {
        this._interval = interval;
        this._getMarketsUrl = getMarketsUrl;
        this._getMarketDataBaseUrl = getMarketDataBaseUrl;
    }

    getMarketsUrl() {
        return this._getMarketsUrl;
    }

    getMarketDataBaseUrl() {
        return this._getMarketDataBaseUrl;
    }

    /**
        @param {Object} - params to build query string
    */
    buildQueryUrl(params) {
        return querystring.stringify(params);
    }

    getIntervalParam() {
        return config.intervalToExchangeInput[[config.exchange]][[this._interval]];
    }

    /**
        Process the OHLC market data by either updating the entity or
        creating it.
        @param {@Object} marketData - object which has 'symbol' and ohlc array data
    */
    processResultMarketData(marketData) {
        var ohlcDataSet = this.convertResponseToCommonModel(marketData.result);
        var params = {
            symbol: marketData.symbol,
            exchange: config.exchange,
            interval: config.interval
        };
        this.addMarketData(params, ohlcDataSet);
    }

    /**
        save a new stock entity or update an existing one
        with latest OHLC dataset.
        @param {@Object} params - filter to find document
        @param {@Object[]} params - OHLC dataset
    */
    async addMarketData(params, ohlcDataSet) {
        if (ohlcDataSet === null || ohlcDataSet.length === 0) {
            logger.error("Empty ohlcDataSet for params: " + JSON.stringify(params));
            return;
        }
        this.popMostRecentDataIfNeeded(ohlcDataSet);
        var docKey = Utils.buildStockEntityDocumentId(params);
        var lastMarketData = await QueryHelper.getFirstBySorted(params, -1);
        if (_.isEmpty(lastMarketData)) {
            if (ohlcDataSet.length === 1) {
                var entity = {
                    s: params.symbol,
                    e: params.exchange,
                    i: params.interval,
                    msi: docKey,
                    m: ohlcDataSet[0]
                };
                QueryHelper.save(entity);
                return;
            }
            QueryHelper.bulkSave(params, ohlcDataSet);
        } else {
            this.addNewMarketData(params, lastMarketData.m.t, ohlcDataSet);
        }
    }

    /**
        Pop most recent ohlc data since that time interval hasn't finished yet.
        Return if head elem was removed, otherwise return empty object.
        @param {@Object[]} ohlcDataSet
    */
    popMostRecentDataIfNeeded(ohlcDataSet) {
        var lastItem = _.last(ohlcDataSet);
        var lastDateTime = DateTime.fromMillis(lastItem.t, {zone: "UTC"});
        var nowDate = DateTime.fromMillis(new Date().getTime(), {zone: "UTC"});
        if (Utils.getDifferenceByInterval(lastDateTime, nowDate,
                                          config.intervalToDateTimeDiffMap[[config.interval]]) <= 1) {
            var removedElem = ohlcDataSet.pop();
            logger.info("Removed elem from ohlcDataSet: " + JSON.stringify(removedElem));
            return removedElem;
        }
        return {};
    }

    /**
        update the given stock entity if needed by checking if there are any OHLC data set to add.
        @param {@Object} params - filter to find document
        @param {@Number} lastTime - time in milliseconds
        @param {@Object[]} ohlcDataSet - OHLC dataset
    */
    addNewMarketData(params, lastTime, ohlcDataSet) {
        var startDate = DateTime.fromMillis(lastTime, {zone: "UTC"});
        var prevDataList = Utils.getPrevDataListWithMillis(startDate, ohlcDataSet,
                                config.intervalToDateTimeDiffMap[[config.interval]]);
        if (_.isEmpty(prevDataList)) {
            return;
        }
        QueryHelper.bulkSave(params, prevDataList);
    }

    /**
        @param {string} symbol
    */
    buildSymbolParam(symbol) {
        throw new Error("This method should been implemented by implementation class");
    }

    /**
        Build the url to get market ticker for given symbol with interval
        @param {string} symbol
    */
    buildGetMarketTickerUrl(symbol) {
        throw new Error("This method should been implemented by implementation class");
    }

    /**
        @param {Object[]} marketTickerList
    */
    convertResponseToCommonModel(marketTickerList) {
        throw new Error("This method should been implemented by implementation class");
    }

    /**
        Get list of active markets from bittrex.
    */
    getMarketList() {
        throw new Error("This method should been implemented by implementation class");
    }

    /**
        Get OHLC data for given symbol.
        @param {string} symbol
    */
    requestMarketData(symbol) {
        throw new Error("This method should been implemented by implementation class");
    }
}

module.exports = ExchangeHandler;
