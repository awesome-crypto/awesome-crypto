"use strict";

const request = require("request");
const logger = require("../../log/logger.js").getDefaultLogger();
var ExchangeHandler = require("./exchange-handler.js");

/**
    Handler for "bittrex"
*/
class BittrexHandler extends ExchangeHandler {
    constructor(interval) {
        super("https://bittrex.com/api/v1.1/public/getmarkets",
                "https://bittrex.com/Api/v2.0/pub/market/GetTicks",
                    interval);
    }

    /**
        Ex; https://bittrex.com/Api/v2.0/pub/market/GetTicks?marketName=BTC-STRAT&tickInterval=day
        @override
    */
    buildGetMarketTickerUrl(symbol) {
        return this.getMarketDataBaseUrl() + "?"
                + this.buildQueryUrl({marketName: this.buildSymbolParam(symbol),
                                      tickInterval: this.getIntervalParam()});
    }

    /**
        Ex; "BTC-VTC"
        @override
    */
    buildSymbolParam(symbol) {
        return "BTC-" + symbol;
    }

    /**
        Ex;
        {
            O: 0.000748,
            H: 0.00075,
            L: 0.00074332,
            C: 0.000746,
            V: 2969.47856613,
            T: "2018-02-26T04:00:00",
            BV: 2.22017095
        }
        @override
    */
    convertResponseToCommonModel(marketTickerList) {
        var result = [];
        marketTickerList.forEach(function(data) {
            result.push({
                o: data.O,
                h: data.H,
                l: data.L,
                c: data.C,
                bv: data.BV,
                t: new Date(data.T + "Z").getTime()
            });
        });
        return result;
    }

    /**
        @override
    */
    getMarketList() {
        var url = this.getMarketsUrl();
        return new Promise(function(resolve, reject) {
            request(url, function(err, res, body) {
                if (err) {
                    return reject(err);
                }
                var markets = JSON.parse(body).result;
                var marketList = [];
                markets.forEach(function (market) {
                    if (market.BaseCurrency === "BTC" && market.IsActive) {
                        marketList.push(market.MarketCurrency);
                    }
                });
                resolve(marketList);
            });
        });
    }

    /**
        @override
    */
    requestMarketData(symbol) {
        var url = this.buildGetMarketTickerUrl(symbol);
        return new Promise(function(resolve, reject) {
            request(url, function(err, res, body) {
                if (err) {
                    logger.error("Error while requesting url: " + url + " with error: " + err);
                    return reject(err);
                }
                logger.info("GET: " + url);
                var result = [];
                try {
                    result = JSON.parse(body).result;
                } catch(err) {
                    logger.error("Error while parsing request body: " + body
                        + "\tResponse: " + JSON.stringify(res) + "\t" + err);
                    return resolve(result);
                }
                if (result === null || result.length === 0) {
                    logger.error("Empty or Null response. Body: " + body + "\tResponse: " + JSON.stringify(res));
                }
                return resolve(result);
            });
        });
    }
}

module.exports = BittrexHandler;
