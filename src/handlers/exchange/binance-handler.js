"use strict";

const request = require("request");
const logger = require("../../log/logger.js").getDefaultLogger();
const QueryHelper = require("../query-helper.js");
const config = require("../../../config.js");
var ExchangeHandler = require("./exchange-handler.js");

/**
    Handler for "binance" exchange
*/
class BinanceHandler extends ExchangeHandler {
    constructor(interval) {
        super("https://api.binance.com/api/v1/exchangeInfo",
                "https://api.binance.com/api/v1/klines",
                interval);
    }

    /**
        Ex; ohlc data
        [
          [
            1499040000000,      // Open time
            "0.01634790",       // Open
            "0.80000000",       // High
            "0.01575800",       // Low
            "0.01577100",       // Close
            "148976.11427815",  // Volume
            1499644799999,      // Close time
            "2434.19055334",    // Quote asset volume
            308,                // Number of trades
            "1756.87402397",    // Taker buy base asset volume
            "28.46694368",      // Taker buy quote asset volume
            "17928899.62484339" // Ignore
          ]
        ]
        @override
    */
    convertResponseToCommonModel(marketTickerList) {
        var result = [];
        marketTickerList.forEach(function(data) {
            result.push({
                o: data[1],
                h: data[2],
                l: data[3],
                c: data[4],
                bv: data[7],
                t: data[0]
            });
        });
        return result;
    }

    /**
        Ex; binance ticker url examples;
            https://api.binance.com/api/v1/klines?symbol=STRATBTC&interval=1h
            https://api.binance.com/api/v1/klines?symbol=STRATBTC&interval=1h&limit=1000
            https://api.binance.com/api/v1/klines?symbol=STRATBTC&interval=1h&endTime=1514883600000
        @override

    */
    async buildGetMarketTickerUrl(symbol) {
        if (symbol === null) {
            logger.error("Symbol can not be null. Symbol: " + symbol);
            throw new Error("Symbol can not be null. Symbol: " + symbol);
        }
        var lastData = await QueryHelper.getFirstBySorted({
                                    symbol: "BTC-" + symbol,
                                    exchange: config.exchange,
                                    interval: config.interval
                                }, -1).catch(err => {
                                    logger.err(err);
                                });
        var params = {
            symbol: this.buildSymbolParam(symbol),
            interval: this.getIntervalParam()
        };
        if (lastData !== null) {
            params["startTime"] = lastData.m.t;
        }
        return this.getMarketDataBaseUrl() + "?" + this.buildQueryUrl(params);
    }

    /**
        @override
    */
    async requestMarketData(symbol) {
        var url = await this.buildGetMarketTickerUrl(symbol);
        return new Promise(function(resolve, reject) {
            request(url, function(err, res, body) {
                if (err) {
                    logger.error(err);
                    return reject(err);
                }
                logger.info("GET: " + url);
                return resolve(JSON.parse(body));
            });
        });
    }

    /**
        @override
    */
    getMarketList() {
        var url = this.getMarketsUrl();
        return new Promise(function(resolve, reject) {
            request(url, function(err, res, body) {
                if (err) {
                    return reject(err);
                }
                var markets = JSON.parse(body).symbols;
                var marketList = [];
                markets.forEach(function (market) {
                    if (market.status === "TRADING" && market.quoteAsset === "BTC") {
                        marketList.push(market.baseAsset);
                    }
                });
                resolve(marketList);
            });
        });
    }

    /**
        Ex; "VTCBTC"
    */
    buildSymbolParam(symbol) {
        return symbol + "BTC";
    }
}

module.exports = BinanceHandler;
