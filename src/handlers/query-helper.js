"use strict";

var MarketDataModel = require("../database/schema.js");
var Utils = require("./utils.js");
var logger = require("../log/logger.js").getDefaultLogger();

/**
    Helper class for queries.
*/
class QueryHelper {

    static findOne(query, callback) {
        MarketDataModel.findOne(query, null, function(err, stock) {
            if (err) {
                logger.error(err);
                throw Error(err);
            }
            return callback(err, stock);
        });
    }

    /**
        @param {@Object} stockEntity
    */
    static save(stockEntity) {
        var stockModel = new MarketDataModel(stockEntity);
        stockModel.save()
            .then(doc => {
                logger.info("Save operation successfull for stock: " + JSON.stringify(doc));
            }).catch(error => {
                logger.info("Error during Save operation: " + error);
            });
    }

    /**
        Do bulk write operation
        @param {@Object} params
        @param {@Object[]} ohlcDataSet - from exchange.
    */
    static bulkSave(params, ohlcDataSet) {
        var bulkWriteQuery = [];
        ohlcDataSet.forEach(function(ohclData) {
            bulkWriteQuery.push({
                insertOne: {
                    document: {
                        s: params.symbol,
                        e: params.exchange,
                        i: params.interval,
                        msi: Utils.buildStockEntityDocumentId(params),
                        m: ohclData
                    }
                }
            });
        });
        MarketDataModel.bulkWrite(bulkWriteQuery, {ordered: false})
            .then(bulkWriteOpResult => {
                logger.info("BulkWrite operation successfull with result: " + JSON.stringify(bulkWriteOpResult));
            })
            .catch(err => {
                logger.error("Error while bulkWrite operation: " + JSON.stringify(err));
            });
    }

    /**
        find one document and update it.
        @param {@Object} filter - filter to find document
        @param {@Object} update - update object to apply on document
    */
    static findOneAndUpdate(filter, update) {
        MarketDataModel.findOneAndUpdate(filter, update)
            .then(doc => {
                logger.info("Update operation successfull with result: " + JSON.stringify(doc));
            }).catch(err => {
                logger.error(err);
            });
    }

    /**
        Get first stock market data by ascending/descending order from given param object which contains;
         - exchange
         - symbol
         - interval

        @param {@Object} params
        @params {@Number} sortBy - '-1' for desc, '1' for asc order.
        return first/latest stock market data from db.
    */
    static getFirstBySorted(params, sortBy) {
        return MarketDataModel
            .find({ msi: Utils.buildStockEntityDocumentId(params) })
            .sort({"m.t": sortBy})
            .limit(1)
            .then(result => {
                if (result == null || result.length === 0) {
                    return null;
                }
                return result[0];
            })
            .catch(error => {
                logger.error("Error: " + error);
                throw Error(error);
            });
    }
}

module.exports = QueryHelper;
