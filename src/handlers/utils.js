"use strict";

var DateTime = require("luxon").DateTime;
var _ = require("lodash");

class Util {

    /**
        Compare and return difference by hours.
        @param {@DateTime} startDate
        @param {@DateTime} endDate
        @param {@String} interval - indicating the given time interval to get difference.
            Example values; 'hours', 'days'
    */
    static getDifferenceByInterval(startDate, endDate, interval) {
        if (_.isEmpty(startDate) || _.isEmpty(endDate)) {
            throw Error("null objects");
        }
        var diffInInterval = endDate.diff(startDate, interval);
        return diffInInterval.toObject()[[interval]];
    }

    /**
        Get Date object and return DateTime object with UTC timezone.
        @param {@Date} date.
    */
    static convertDateToDateTimeInUTC(date) {
        return DateTime.fromISO(new Date(date).toISOString()).toUTC();
    }

    /**
        Get list of entities by comparing time of last
        entity from db with entity list from exchange.
        @param {@DateTime} lastDateTime
        @param {@Object[]} dataList - the list of market data coming from exchange.
        @param {@String} interval
    */
    static getPrevDataListWithMillis(lastDateTime, dataList, interval) {
        if (lastDateTime === null || dataList.length === 0 || interval === null || interval === undefined) {
            throw Error("null/empty lastDateTime/dataList/interval parameter");
        }
        var result = [];
        for (var i = dataList.length - 1; i >= 0; i--) {
            var dataDateTime = DateTime.fromMillis(dataList[i].t, {zone: "UTC"});
            var diffInHours = this.getDifferenceByInterval(lastDateTime, dataDateTime, interval);
            if (diffInHours >= 1) {
                result.unshift(dataList[i]);
            } else {
                break;
            }
        }
        return result;
    }

    /**
        Build market data document id from given parameters.
        Ex; exchange: bittrex, symbol: BTC-STRAT, interval: 1h
                then return bittrex_BTC-STRAT_1h
        @param {@Object} params
    */
    static buildStockEntityDocumentId(params) {
        if (_.isEmpty(params)) {
            throw Error("Can not build document id with invalid param object: " + JSON.stringify(params));
        }
        return [params.exchange, params.symbol, params.interval].join("_");
    }
}

module.exports = Util;
