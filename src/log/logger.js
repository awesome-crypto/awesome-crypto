"use strict";

var Log = require("log");
var fs = require("fs");
var dir = "./logs";
const config = require("../../config.js");

var loggerMap = {};

class LoggerFactory {
    static getDefaultLogger() {
        this._createDir();
        if (this.logger === null || this.logger === undefined) {
            this.logger = new Logger([config.exchange, config.interval, "application"].join("-"));
        }
        return this.logger;
    }

    static getLogger(operationType) {
        if (loggerMap[operationType]) {
            return loggerMap[operationType];
        }
        loggerMap[operationType] = new Logger(operationType);
        return loggerMap[operationType];
    }

    /**
        Create "./logs" directory if it doesn"t exist.
    */
    static _createDir() {
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }
    }
}

class Logger {

    constructor(operationType) {
        this.logger = new Log("debug", fs.createWriteStream("logs/" + operationType + ".log"), {flags: "a"});
    }

    info(message) {
        this.logger.info(message);
    }

    error(message) {
        this.logger.error(message);
    }
}

module.exports = LoggerFactory;
