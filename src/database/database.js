"use strict";

let mongoose = require("mongoose");
var config = require("../../config.js");
var logger = require("../log/logger.js").getDefaultLogger();

const server = config.server;
const database = config.database;

class Database {

    static initDb() {
        mongoose.connect(`mongodb://${server}/${database}`)
            .then(() => {
                logger.info("Database connection created succesfully");
            }).catch(err => {
                logger.error("Database connection error: ", err);
                throw err;
            });
    }
}

module.exports = Database;
