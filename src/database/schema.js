"use strict";

var mongoose = require("mongoose");

var marketDataModel = new mongoose.Schema({
    msi: String, // key of the document by combining exchange name, symbol and interval
    s: String, // symbol
    e: String, // exchange name
    i: String, // interval
    m: {
        o: Number,
        h: Number,
        l: Number,
        c: Number,
        t: Number,
        bv: Number,
        _id: false
    }
}, {collection: "marketData"});

module.exports = mongoose.model("MarketDataModel", marketDataModel);
