"use strict";

var BittrexHandler = require("../handlers/exchange/bittrex-handler.js");
var BinanceHandler = require("../handlers/exchange/binance-handler.js");

/**
    Factory class for exchange handlers.
*/
class ExchangeHandlerFactory {

    /**
        @param {@String} exchangeName
        @param {@String} interval
    */
    static getExchangeHandler(exchangeName, interval) {
        if (this._exchangeHandler != null) {
            return this._exchangeHandler;
        }

        if (exchangeName === "bittrex") {
            this._exchangeHandler = new BittrexHandler(interval);
            return this._exchangeHandler;
        } else if (exchangeName === "binance") {
            this._exchangeHandler = new BinanceHandler(interval);
            return this._exchangeHandler;
        }
        throw new Error("Exchange name undefined");
    }
}

module.exports = ExchangeHandlerFactory;
